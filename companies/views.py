from django.http import JsonResponse
from django.shortcuts import render

from companies.forms import CompanyForm
from companies.models import Company


def index(request):
    companies = Company.objects.all()
    if request.method == 'POST':
        form = CompanyForm(request.POST)
        if form.is_valid():
            company = form.save()
            return JsonResponse({
                'status': 'success',
                'message': 'New task created'
            })
        else:
            return JsonResponse({
                'status': 'error',
                'message': form.errors
            })
    else:
        form = CompanyForm()
    return render(
        request,
        'projects/index.html',
        {'companies': companies, 'addCompanyForm': form})

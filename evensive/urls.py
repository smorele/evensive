from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'evensive.views.home', name='home'),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^projects/', include('projects.urls')),
    url(r'^members/', include('members.urls')),
    url(r'^companies/', include('companies.urls')),

]

$(document).ready(function(){
    /*  from https://docs.djangoproject.com/en/1.8/ref/csrf/ */
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $(".reveal-modal form").submit(function(event) {
        event.preventDefault();
        var formId = $(this).attr('id');
        var currentUrl = $(this).data('url');
        $.ajax({
            url: currentUrl,
            type: 'POST',
            data: $(this).serialize(),
            success: function(data, textStatus, jqXHR){
                if('error' == data['status']){
                    $.each(data['message'], function(err, mess){
                        $("[name="+err+"]").addClass('error').after("<span class='error'>"+mess+"</span>");
                    });
                }else {
                    if("addCompanyForm" == formId){
                        $('#addCompanyModal').foundation('reveal', 'close');
                        $('#addProjectModal').foundation('reveal', 'open');
                    }else{
                        window.location.href = currentUrl;
                    }
                }
            }
        });
    });



});
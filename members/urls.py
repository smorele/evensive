from django.conf.urls import patterns, url

from members import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='members'),
    url(r'^(?P<member_id>\d+)$', views.detail, name='member_detail'),
)
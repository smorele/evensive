from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render

from members.models import Member
from members.forms import MemberForm


def index(request):
    members = Member.objects.all()
    if request.method == 'POST':
        form = MemberForm(request.POST)
        if form.is_valid():
            member = form.save()
            return JsonResponse({
                'status': 'success',
                'message': 'New task created'
            })
        else:
            return JsonResponse({
                'status': 'error',
                'message': form.errors
            })
    else:
        form = MemberForm()

    return render(
        request,
        'index.html',
        {'members': members, 'addMemberForm': form}
    )


def detail(request, member_id):
    member = get_object_or_404(Member, pk=member_id)
    return render(
        request,
        'detail.html',
        {'member': member})

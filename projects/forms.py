from django.forms import ModelForm
from projects.models import Milestone, Project, Task

class MilestoneForm(ModelForm):
    class Meta:
        model = Milestone
        fields = ['name', 'due_date']

class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = '__all__'
        exclude = ['project', 'created_by', 'description']


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = '__all__'
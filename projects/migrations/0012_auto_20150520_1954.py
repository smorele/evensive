# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0001_initial'),
        ('projects', '0011_auto_20150520_1941'),
    ]

    operations = [
        migrations.CreateModel(
            name='Action',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('elaped_time', models.IntegerField()),
                ('progession', models.IntegerField()),
                ('member', models.ForeignKey(to='members.Member')),
                ('task', models.ForeignKey(to='projects.Task')),
            ],
        ),
        migrations.AddField(
            model_name='task',
            name='actions',
            field=models.ManyToManyField(to='members.Member', through='projects.Action'),
        ),
    ]

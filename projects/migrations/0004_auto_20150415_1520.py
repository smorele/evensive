# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0003_milestone_task'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='task',
            name='milesone',
        ),
        migrations.AddField(
            model_name='task',
            name='milestone',
            field=models.ForeignKey(blank=True, to='projects.Milestone', null=True),
        ),
    ]

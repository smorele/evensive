# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0004_auto_20150415_1520'),
    ]

    operations = [
        migrations.AddField(
            model_name='milestone',
            name='project',
            field=models.ForeignKey(default=None, to='projects.Project'),
            preserve_default=False,
        ),
    ]

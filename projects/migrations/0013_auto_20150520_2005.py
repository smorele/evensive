# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0012_auto_20150520_1954'),
    ]

    operations = [
        migrations.RenameField(
            model_name='action',
            old_name='elaped_time',
            new_name='elapsed_time',
        ),
    ]

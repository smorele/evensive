# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0001_initial'),
        ('projects', '0007_milestone_project'),
    ]

    operations = [
        migrations.AddField(
            model_name='milestone',
            name='assign_to',
            field=models.ForeignKey(related_name='assign_to', default=None, to='members.Member'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='milestone',
            name='created_by',
            field=models.ForeignKey(related_name='created_by', default=None, to='members.Member'),
            preserve_default=False,
        ),
    ]

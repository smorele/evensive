# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0001_initial'),
        ('projects', '0008_auto_20150418_1247'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='milestone',
            name='assign_to',
        ),
        migrations.AddField(
            model_name='task',
            name='assign_to',
            field=models.ForeignKey(related_name='assign_to', default=None, to='members.Member'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='task',
            name='created_by',
            field=models.ForeignKey(related_name='task_created_by', default=None, to='members.Member'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='milestone',
            name='created_by',
            field=models.ForeignKey(related_name='miletone_created_by', to='members.Member'),
        ),
    ]

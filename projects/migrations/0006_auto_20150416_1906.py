# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0005_milestone_project'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='milestone',
            name='project',
        ),
        migrations.AddField(
            model_name='task',
            name='project',
            field=models.ForeignKey(default=None, to='projects.Project'),
            preserve_default=False,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0010_auto_20150515_2057'),
    ]

    operations = [
        migrations.RenameField(
            model_name='task',
            old_name='assign_to',
            new_name='assigned_to',
        ),
        migrations.AddField(
            model_name='task',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 20, 19, 41, 38, 718230, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]

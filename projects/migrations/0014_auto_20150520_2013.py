# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0013_auto_20150520_2005'),
    ]

    operations = [
        migrations.RenameField(
            model_name='action',
            old_name='progession',
            new_name='progression',
        ),
    ]

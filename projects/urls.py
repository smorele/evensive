from django.conf.urls import url

from projects import views

urlpatterns = [
    url(r'^$', views.index, name='projects'),
    url(r'^(?P<project_id>[0-9]+)/overview$', views.overview, name='project_overview'),
    url(r'^(?P<project_id>[0-9]+)/tasks$', views.tasks, name='project_tasks'),
    url(r'^(?P<project_id>[0-9]+)/tasks/(?P<task_id>[0-9]+)$', views.task, name='project_task'),
    url(r'^(?P<project_id>[0-9]+)/milestones$', views.milestones, name='project_milestones'),
    url(r'^(?P<project_id>[0-9]+)/milestones/(?P<milestone_id>[0-9]+)$', views.milestone, name='project_milestone')

]
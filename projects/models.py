from django.db import models

from companies.models import Company
from members.models import Member

class Project(models.Model):
    name = models.CharField(max_length=50)
    company = models.ForeignKey(Company)

    def __str__(self):
        return self.name

    def get_total_duration(self):
        duration = 0
        for task in self.task_set.all():
            duration += task.duration

        return duration

class Milestone(models.Model):
    name = models.CharField(max_length=125)
    due_date = models.DateField()
    project = models.ForeignKey(Project)
    created_by = models.ForeignKey(Member, related_name="miletone_created_by")

    def __str__(self):
        return self.name


class Task(models.Model):
    name = models.CharField(max_length=125)
    project = models.ForeignKey(Project)
    description = models.TextField()
    milestone = models.ForeignKey(Milestone, blank=True, null=True)
    duration = models.IntegerField()
    assigned_to = models.ForeignKey(Member, related_name="assign_to", blank=True, null=True)
    created_by = models.ForeignKey(Member, related_name="task_created_by")
    creation_date = models.DateTimeField(auto_now_add=True)
    actions = models.ManyToManyField(Member, through="Action")

    def __str__(self):
        return self.name


class Action(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    task = models.ForeignKey(Task)
    member = models.ForeignKey(Member)
    elapsed_time = models.IntegerField()
    progression  = models.IntegerField()


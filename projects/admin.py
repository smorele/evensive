from django.contrib import admin
from projects.models import Action, Project, Milestone, Task

admin.site.register(Project)
admin.site.register(Milestone)
admin.site.register(Task)
admin.site.register(Action)



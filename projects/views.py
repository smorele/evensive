from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render, redirect

from companies.forms import CompanyForm
from members.models import Member
from projects.forms import MilestoneForm, ProjectForm, TaskForm
from projects.models import Milestone, Project, Task


def index(request):
    projects = Project.objects.all()
    companyForm = CompanyForm()
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save()
            return JsonResponse({
                'status': 'success',
                'message': 'New task created'
            })
        else:
            return JsonResponse({
                'status': 'error',
                'message': form.errors
            })
    else:
        form = ProjectForm()
    return render(
        request,
        'projects/index.html',
        {'projects': projects, 'addProjectForm': form, 'addCompanyForm': CompanyForm})

def overview(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    return render(
        request,
        'projects/overview.html',
        {'project': project})


def tasks(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    tasks = Task.objects.filter(project=project)

    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.project = project
            task.created_by = Member.objects.get(pk=1)
            task.save()
            return JsonResponse({
                'status': 'success',
                'message': 'New task created'
            })
        else:
            return JsonResponse({
                'status': 'error',
                'message': form.errors
            })
    else:
        form = TaskForm()

    return render(
        request,
        'projects/tasks.html',{
            'project': project,
            'addTaskForm': form,
            'tasks': tasks
        })

def task(request, project_id, task_id):
    project = get_object_or_404(Project, pk=project_id)
    task = get_object_or_404(Task, pk=task_id)

    return render(
        request,
        'projects/task.html',{
            'project': project,
            'task': task
        })

def milestones(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    milestones = Milestone.objects.filter(project=project)

    if request.method == 'POST':
        form = MilestoneForm(request.POST)
        if form.is_valid():
            milestone = form.save(commit=False)
            milestone.project = project
            milestone.created_by = Member.objects.get(pk=1)
            milestone.save()
            return JsonResponse({
                'status': 'success',
                'message': 'New milestone created'
            })
        else:
            return JsonResponse({
                'status': 'error',
                'message': form.errors
            })
    else:
        form = MilestoneForm()

    return render(
        request,
        'projects/milestones.html',{
            'project': project,
            'addMilestoneForm': form,
            'milestones': milestones
        })

def milestone(request, project_id, milestone_id):
    project = get_object_or_404(Project, pk=project_id)
    milestone = get_object_or_404(Milestone, pk=milestone_id)

    return render(
        request,
        'projects/milestone.html',{
            'project': project,
            'milestone': milestone
        })